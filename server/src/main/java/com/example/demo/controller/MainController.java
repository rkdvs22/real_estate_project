package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.service.UserService;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
@Controller
public class MainController {
	
	@Autowired UserService userService;

	MongoClient mongoClient = null;
    DB db=null;

	@RequestMapping("/welcome")
	public String welcome() {
		return "welcome";

	}

	@RequestMapping(value = "/getUserInfo")
	public String getUserInfo(user user, Model model) {
		int userId = user.getUserId();
		String userName = user.getUserName();
		model.addAttribute("userId", userId);
		model.addAttribute("userName", userName);
		return "userInfo";
	}

	@RequestMapping("/test")
	public ModelAndView test() throws Exception {
		ModelAndView mav = new ModelAndView("test");
		mav.addObject("name", "testcheck");
		List<String> testList = new ArrayList<String>();
		testList.add("a");
		testList.add("b");
		testList.add("c");
		mav.addObject("list", testList);
		MainController testRunner = new MainController();
		testRunner.mongoTest("localhost", 27017, "testdb");
		
		return mav;
	}

	public void mongoTest(String ip, int port, String dbname) throws Exception {
		mongoClient = new MongoClient(new ServerAddress(ip, port));
		db = mongoClient.getDB(dbname);
		DBCollection userTable = db.getCollection("userTable");
		BasicDBObject doc = new BasicDBObject("name", "MongoDB").append("type", "database").append("count", 1)
				.append("info", new BasicDBObject("x", 203).append("y", 102));
		userTable.insert(doc);
	}

	@RequestMapping("/userSelect")
	public ModelAndView select() throws Exception {
		ModelAndView mav = new ModelAndView("test");
		mav.addObject("name", "testselect");
		List<String> testList = new ArrayList<String>();
		testList.add("s");
		String result=userService.userSelect("localhost", 27017, "testdb", mongoClient, db);
		
		testList.add(result);
		mav.addObject("list", testList);
		
		return mav;
	}
	@RequestMapping("/userInsert")
	public ModelAndView insert() throws Exception {
		ModelAndView mav = new ModelAndView("test");
		mav.addObject("name", "testinsert");
		List<String> testList = new ArrayList<String>();
		testList.add("i");
		String result=userService.userInsert("localhost", 27017, "testdb", mongoClient, db);
		
		testList.add(result);
		mav.addObject("list", testList);
		
		return mav;
	}
	@RequestMapping("/map")
	public ModelAndView map() throws Exception {
		ModelAndView mav = new ModelAndView("test");
		mav.addObject("name", "map");
		List<String> testList = new ArrayList<String>();
		/*testList.add("i");
		String result=userService.userInsert("localhost", 27017, "testdb", mongoClient, db);
		
		testList.add(result);*/
		mav.addObject("list", testList);
		
		return mav;
	}
}