package com.example.demo.dao;

import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
	
	/*@Autowired //@Resource
	private SqlSessionTemplate sqlSession;

	public List<BoardDto> selectNoticeBoardList(BoardDto boardDto) {
		return sqlSession.selectList("noticeBoard.selectNoticeBoardList", boardDto);
	}*/
	private int userId;
	private String userName;
	private String userPhone;
	
	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}