package com.example.demo.service;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public interface UserService {
	//public List<BoardDto> selectNoticeBoardList(HttpServletRequest request, BoardDto boardDto) throws Exception;

	public String userSelect(String ip, int port, String dbname,MongoClient mongoClient, DB db) throws Exception;
	public String userInsert(String ip, int port, String dbname,MongoClient mongoClient, DB db) throws Exception;
}
