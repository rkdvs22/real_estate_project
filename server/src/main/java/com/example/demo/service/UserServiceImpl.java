package com.example.demo.service;

import java.time.LocalDateTime;

import javax.print.attribute.standard.DateTimeAtCompleted;

import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

@Service
public class UserServiceImpl implements UserService {

//	@Autowired //@Resource
	//private BoardDao boardDao;

	//public List<BoardDto> selectNoticeBoardList(HttpServletRequest request, BoardDto boardDto) throws Exception {
		//return boardDao.selectBoardList(boardDto);
	//}
	@Override
	public String userSelect(String ip, int port, String dbname,MongoClient mongoClient, DB db) throws Exception {
		try {
		mongoClient = new MongoClient(new ServerAddress(ip, port));
		db = mongoClient.getDB(dbname);
		DBCollection userTable = db.getCollection("userTable");
		BasicDBObject doc = new BasicDBObject("name", "MongoDB").append("type", "database").append("count", 1)
				.append("info", new BasicDBObject("x", 203).append("y", 102));
		userTable.insert(doc);
		/* MongoClient mClient = new MongoClient("localhost", 27017);
        MongoDatabase helloDB = mClient.getDatabase("hellomongo");
        MongoCollection<Document> collection = helloDB.getCollection("employees");
//        Document document = new Document("empno", "8888")
//                .append("ename", "t_name")
//                .append("job", "t_job")
//                .append("hiredate", "03-01-2018")
//                .append("sal", 50)
//                .append("deptno", 0);
//        collection.insertOne(document);
        
//        Document doc1 = new Document("empno", "9999")
//                .append("ename", "t_name")
//                .append("job", "t_job")
//                .append("hiredate", "03-01-2018")
//                .append("sal", 50)
//                .append("deptno", 1);
//         
//        
//        Document doc2 = new Document("empno", "5555")
//                .append("ename", "t_name")
//                .append("job", "t_job")
//                .append("hiredate", "03-01-2018")
//                .append("sal", 50)
//                .append("deptno", 2);         
//        
//        Document doc3 = new Document("empno", "2222")
//                .append("ename", "t_name")
//                .append("job", "t_job")
//                .append("hiredate", "03-01-2018")
//                .append("sal", 50)
//                .append("deptno", 3);
//        List<Document> docs = new ArrayList<>();
//        docs.add(doc1);
//        docs.add(doc2);
//        docs.add(doc3);
//        collection.insertMany(docs);
//        collection.find().forEach(printBlock);
        
        //Update
//        collection.updateOne(    //데이터 하나만 바꿈
//                Filters.eq("empno", "2222"),    //조건
//                // combine : 수정할 내용
//                Updates.combine(Updates.set("ename", "up_name"),
//                // currentDate : 수정할 시간
//                Updates.currentDate("lastModifyfield")),
//                                //맞는 조건이 없으면 필드 insert //제약조건 유지. 
//                new UpdateOptions().upsert(true).bypassDocumentValidation(true)    
//        );    
        
        //조건에 맞는 여러 document 수정
//        collection.updateMany(
//                Filters.eq("empno", "9999"),
//                Updates.combine(Updates.set("sal", 0), Updates.currentDate("lastModifyField"))                
//        );
//        collection.find().forEach(printBlock);
        
        //save (replaceOne)
        //update가 필드단위의 수정이라면,
        //save는 document단위의 수정    
        //동일한 Document의 필드를 바꿈
//        collection.replaceOne(
//                Filters.eq("empno", "2222"),
//                new Document("ename", "s_name")
//                .append("sal", 1000)
//                .append("dept", 100)
//        );
//        collection.find().forEach(printBlock);
        //조건에 부합하는 것 한개만 삭제
//        collection.deleteOne(Filters.eq("enano", "8888"));
        //조건에 부합하는 것 모두 삭제
        collection.deleteMany(Filters.eq("ename", "s_name"));
        collection.find().forEach(printBlock);
        
        mClient.close();*/
		return "OK";
		}catch (Exception e) {
			throw e;
		}
	}
	@Override
	public String userInsert(String ip, int port, String dbname,MongoClient mongoClient, DB db) throws Exception {
		try {
		mongoClient = new MongoClient(new ServerAddress(ip, port));
		db = mongoClient.getDB(dbname);
		DBCollection userTable = db.getCollection("userTable");
		BasicDBObject doc = new BasicDBObject("name", "MongoDB").append("type", "database").append("count", 1)
				.append("info", new BasicDBObject("x", 203).append("y", 102));
		userTable.insert(doc);
		
		return "OK";
		}catch (Exception e) {
			throw e;
		}
	}
}