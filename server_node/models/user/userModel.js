const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// user테이블 스키마
const UserSchema = new Schema({
  id: "string",
  password: "string",
  email: "string",
  age: "number",
  gender: "string",
  phoneNum: "string",
});

module.exports = mongoose.model("User", UserSchema);
