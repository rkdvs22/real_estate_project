const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// user테이블 스키마
const RealEstateSchema = new Schema({
  name: "string",
  email: "string",
  address: "string",
  googleMapInfo: "string",
  registeredDate: "string",
  modifiedDate: "string",
});

module.exports = mongoose.model("RealEstate", RealEstateSchema);
