const commonFunc = {};

function sleepByPromise(milliSec) {
  return new Promise((resolve) => setTimeout(resolve, milliSec));
}

//#region wait
commonFunc.wait = async function (milliSec) {
  await sleepByPromise(milliSec);
};

/**
 * 날짜 포멧팅
 * @param  {Date}   date     날짜
 * @param  {String} [format] 포멧
 * @return {String}          포멧팅 된 날짜
 */
commonFunc.formatDate = function (date, format) {
  if (!format) format = "YYYY-MM-DD hh:mm:ss.SSS";
  format = format.replace(/YYYY/g, date.getFullYear());
  format = format.replace(/MM/g, ("0" + (date.getMonth() + 1)).slice(-2));
  format = format.replace(/DD/g, ("0" + date.getDate()).slice(-2));
  format = format.replace(/hh/g, ("0" + date.getHours()).slice(-2));
  format = format.replace(/mm/g, ("0" + date.getMinutes()).slice(-2));
  format = format.replace(/ss/g, ("0" + date.getSeconds()).slice(-2));
  if (format.match(/S/g)) {
    var milliSeconds = ("00" + date.getMilliseconds()).slice(-3);
    var length = format.match(/S/g).length;
    for (var i = 0; i < length; i++)
      format = format.replace(/S/, milliSeconds.substring(i, i + 1));
  }
  return format;
};

module.exports = commonFunc;
