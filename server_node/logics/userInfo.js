// logger
const logger = require("../logger/logger");
const User = require('../models/user/userModel');

const userInfo = {};

userInfo.getUserList = async function (req, res) {
  try {
    User.find(function (error, result) {
      if (error !== null) {
        console.log(error);
        res.status(500).send({ data: [], status: "fail", responseCode: 500 });
      } else {
        res.status(200).send({ data: result, status: "success", responseCode: 200 });
      }
    });

  } catch (err) {
    res.status(800).send({ data: [], status: "fail", responseCode: 800 });
    logger.error(err.stack);
    throw err;
  } finally {
    
  }
};

userInfo.addUser = async function (req, res) {
  try {
    const newUser = new User(req.body);

    newUser.save(function (error, result) {
      if (error !== null) {
        console.log(error);
        res.status(500).send({ data: [], status: "fail", responseCode: 500 });
      } else {
        res.status(200).send({ data: result, status: "success", responseCode: 200 });
      }
    });

  } catch (err) {
    res.status(800).send({ data: [], status: "fail", responseCode: 800 });
    logger.error(err.stack);
    throw err;
  } finally {
    
  }
};

module.exports = userInfo;