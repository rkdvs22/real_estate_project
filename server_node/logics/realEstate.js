// logger
const logger = require("../logger/logger");
const RealEstate = require('../models/realEstate/realEstateModel');

const realEstate = {};

realEstate.getRealEstateList = async function (req, res) {
  try {
    RealEstate.find(function (error, result) {
      if (error !== null) {
        console.log(error);
        res.status(500).send({ data: [], status: "fail", responseCode: 500 });
      } else {
        res.status(200).send({ data: result, status: "success", responseCode: 200 });
      }
    });

  } catch (err) {
    res.status(800).send({ data: [], status: "fail", responseCode: 800 });
    logger.error(err.stack);
    throw err;
  } finally {
    
  }
};

module.exports = realEstate;