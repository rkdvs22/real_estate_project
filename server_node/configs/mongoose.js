const config = require("../configs/server-config"),
mongoose = require("mongoose");

module.exports = function () {
  var db = mongoose.connect(config, {useNewUrlParser: true, useUnifiedTopology: true});
  require("../models/user/userModel");
  require("../models/realEstate/realEstateModel");

  mongoose.connection.on('error', function(){
    console.log('Connection Failed!');
  });

  mongoose.connection.once('open', function() {
    console.log('Connected!');
  });
  
  return db;
};
