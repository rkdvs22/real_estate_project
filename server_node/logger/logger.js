const appRoot = require('app-root-path');
const winston = require('winston');
const process = require('process');
const { formatDate } = require('../common/commonFunc');
 
const { combine, printf } = winston.format;
const logName = formatDate(new Date(), 'YYYYMMDD');
const currentDateTime = new Date();
 
const myFormat = printf(({ level, message }) => {
  return `${formatDate(currentDateTime)} [${level}] : ${message}`;
});
 
const options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/${logName}.log`,
    handleExceptions: true,
    json: false,
    maxsize: 625000, // 5MB
    maxFiles: 5,
    colorize: false,
    format: combine(
      myFormat
    )
  },
  console: {
    level: 'error',
    handleExceptions: true,
    json: false,
    colorize: true,
    format: combine(
      myFormat
    )
  }
}
 
let logger = new winston.createLogger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ],
  exitOnError: false, 
});
 
if(process.env.NODE_ENV !== 'production'){
  logger.add(new winston.transports.Console(options.console))
}
 
module.exports = logger;