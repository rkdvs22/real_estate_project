// middleware세팅
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");

//winston logger
const logger = require('./logger/logger');

const app = express();
app.use(express.json({ extended: false, limit: "100mb" }));
app.use(cors());
app.use(
  fileUpload({
    createParentPath: true,
  })
);

// jsonWebToken
const jwt = require("jsonwebtoken");

// port
const port = process.env.PORT || 8007;

const config = require("./configs/server-config");

// mongoDB connect
const db = require('./configs/mongoose');
// 몽고디비 접속 실행
db();


//apiRoute
const apiRoutes = express.Router();
app.use("/api", apiRoutes);

// server 테스트
apiRoutes.get('/healthcheck', function(req, res){
  res.send('server alive!');
});

// 몽고 디비 로직
const userInfo = require('./logics/userInfo');
const realEstate = require('./logics/realEstate');

// restful api 라우팅 정의
apiRoutes.get('/user/get/userList', (req, res) => userInfo.getUserList(req, res));
apiRoutes.post('/user/add/user', (req, res) => userInfo.addUser(req, res));
apiRoutes.get('/getRealEstateList', (req, res) => realEstate.getRealEstateList(req, res));

app.listen(port, () => logger.info(`Start real_state node server listening on port ${port}...`));
