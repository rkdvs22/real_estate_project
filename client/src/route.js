import React from "react";
import { Route, Switch } from "react-router-dom";

import PageNotFound from "./components/pages/routePageNotFound/PageNotFound";
import Main from "./components/pages/main/Main";
import Map from "./components/pages/map/Map";
import Member from "./components/pages/member/Member";

const Routes = () => {
  return (
    <Switch>
      {/* what is main page design? */}
      <Route exact path="/Map" component={Map} />
      <Route exact path="/Member" component={Member} />
      <Route exact path="/" component={Main} />
      
      <Route component={PageNotFound} />
    </Switch>
  );
};

export default Routes;
