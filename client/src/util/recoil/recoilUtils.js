import { atom } from "recoil";

export const NAV_MENU_FLAG = atom({
  key: "nav_flag",
  default: {
    isOpen:false,
    isSetOpen:false
  },
});

