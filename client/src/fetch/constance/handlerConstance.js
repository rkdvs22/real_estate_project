import { authHeader } from "./AuthHeader";

export const commonConstants = {
  API_HOST: "http://localhost:8007/api",
};

export const REQUEST_OPTIONS_GET = {
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Auth: authHeader().Authorization,
  },
};

export const REQUEST_OPTIONS_POST = (parameterObj) => {
  const option = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Auth: authHeader().Authorization,
    },
    body: JSON.stringify(parameterObj),
  }

  return option;
};
