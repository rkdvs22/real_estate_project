// USER
export const URL_GET_USER_INFO = "user/get/user";
export const URL_GET_USER_LIST = "user/get/userList";
export const URL_ADD_USER = "user/add/user";
export const URL_UPDATE_USER = "user/update/user";
export const URL_DELETE_USER = "user/delete/user";

// STAT
