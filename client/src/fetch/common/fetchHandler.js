import { REQUEST_OPTIONS_GET, REQUEST_OPTIONS_POST, commonConstants } from "../constance/handlerConstance";

const FetchHandler = (
  promiseType,
  apiURL,
  parameterObj,
  successCallback,
  failCallback
) => {
  switch (promiseType) {
    case "get":
      getFetchHandler(apiURL, successCallback, failCallback);
      break;
    case "post":
      postFetchHandler(apiURL, parameterObj, successCallback, failCallback);
      break;
    default:
      break;
  }
};

function getFetchHandler(apiURL, successCallback, failCallback = undefined) {

  const apiPath = `${commonConstants.API_HOST}/${apiURL}`;
  fetch(apiPath, REQUEST_OPTIONS_GET)
    .then((response) => response.json())
    .then((response) => {
      if (response.status === "success" && response.responseCode === 200) {
        successCallback(response.data);
      } else {
        //サーバapi通信エラー
        if (failCallback !== undefined) {
          failCallback(response);
        } else {
          // 실패 콜백을 넘겨 주지 않을 경우 처리.
          
        }
      }
    })
    .catch((e) => {
      //ネットワークエラー
      console.log(e);
    });
}

function postFetchHandler(apiURL, parameterObj, successCallback, failCallback = undefined) {

  const apiPath = `${commonConstants.API_HOST}/${apiURL}`;
  fetch(apiPath, REQUEST_OPTIONS_POST(parameterObj))
    .then((response) => response.json())
    .then((response) => {
      if (response.status === "success" && response.responseCode === 200) {
        successCallback(response.data);
      } else {
        //サーバapi通信エラー
        if (failCallback !== undefined) {
          failCallback(response);
        } else {
          // 실패 콜백을 넘겨 주지 않을 경우 처리.
          
        }
      }
    })
    .catch((e) => {
      //ネットワークエラー
      console.log(e);
    });
}

export default FetchHandler;
