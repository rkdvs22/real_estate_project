import FetchHandler from "../../common/fetchHandler";
import { 
  URL_GET_USER_INFO, 
  URL_GET_USER_LIST, 
  URL_ADD_USER, 
  URL_UPDATE_USER, 
  URL_DELETE_USER 
} from "../../path/user/userApiPath";

/**
 * 유저 정보 취득
 * @param {Object} parameterObj
 * @param {string} parameterObj.id  유저 아이디
 * @param {Function} callbackOnSuccess
 * @param {Function} callbackOnFailure
 */
 export const getUserInfo = (
  parameterObj,
  callbackOnSuccess,
  callbackOnFailure
) => {
  FetchHandler(
    "post",
    URL_GET_USER_INFO,
    parameterObj,
    callbackOnSuccess,
    callbackOnFailure
  );
};

/**
 * 유저 리스트 취득
 * @param {Function} callbackOnSuccess
 * @param {Function} callbackOnFailure
 */
 export const getUserList = (
  callbackOnSuccess,
  callbackOnFailure
) => {
  FetchHandler(
    "get",
    URL_GET_USER_LIST,
    {},
    callbackOnSuccess,
    callbackOnFailure
  );
};

/**
 * 유저 등록
 * @param {Function} callbackOnSuccess
 * @param {Function} callbackOnFailure
 */
 export const addUser = (
  parameterObj,
  callbackOnSuccess,
  callbackOnFailure
) => {
  FetchHandler(
    "post",
    URL_ADD_USER,
    parameterObj,
    callbackOnSuccess,
    callbackOnFailure
  );
};