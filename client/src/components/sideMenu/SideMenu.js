import React from "react";
import { Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  ShopOutlined,
  HomeOutlined,
  GlobalOutlined,
  MailOutlined,
  SettingOutlined,
} from "@ant-design/icons";

import "./SideMenu.css";

const SideMenu = (props) => { 
  const { isCollapsed, collapseToggle } = props;

  const iconStyle = {
    fontSize: "18px",
    color: "white",
  };

  const onClickSideMenu=(e)=>{
    var navPage = e.key;
 
    window.location.href = navPage;
  }
     
  return (
    <>
      <div className="navIcon_div">
        {isCollapsed ? (
          <MenuUnfoldOutlined style={iconStyle} onClick={collapseToggle} />
        ) : (
          <MenuFoldOutlined style={iconStyle} onClick={collapseToggle} />
        )}
      </div>
      <Menu
        className="side__menu"
        theme="dark"
        mode="inline"
        // defaultSelectedKeys={["1"]}
        // defaultOpenKeys={["sub1"]}
      >
        <Menu.SubMenu key="sub1" icon={<HomeOutlined />} title="会員情報">
          <Menu.ItemGroup key="g1" title="会員情報">
            <Menu.Item key="/member" onClick={onClickSideMenu}>会員登録</Menu.Item>
            <Menu.Item key="/updateMember" onClick={onClickSideMenu}>会員情報変更</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="g2" title="契約情報">
            <Menu.Item key="/searchContract">契約内容検索</Menu.Item>
            <Menu.Item key="/updateContract">契約情報変更</Menu.Item>
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu key="sub2" icon={<ShopOutlined />} title="業者情報">
          <Menu.Item key="/addDealer" onClick={onClickSideMenu}>業者登録</Menu.Item>
          <Menu.Item key="/updateDealer" onClick={onClickSideMenu}>業者情報変更</Menu.Item>
          <Menu.SubMenu key="sub3" title="取引情報">
            <Menu.Item key="/addAccount" onClick={onClickSideMenu}>口座情報登録</Menu.Item>
            <Menu.Item key="/pointTransaction" onClick={onClickSideMenu}>ポイント取引</Menu.Item>
          </Menu.SubMenu>
        </Menu.SubMenu>
        <Menu.SubMenu key="sub4" icon={<GlobalOutlined />}  title="地図">
          <Menu.Item key="/map" className="map" onClick={onClickSideMenu}>地図から検索</Menu.Item>
          <Menu.SubMenu key="sub5" title="準備中">
            <Menu.Item key="10">準備中</Menu.Item>
          </Menu.SubMenu>
        </Menu.SubMenu>
        <Menu.SubMenu key="sub6" icon={<MailOutlined />} title="管理者にメール"></Menu.SubMenu>
        <Menu.SubMenu key="sub7" icon={<SettingOutlined />} title="設定">
          <Menu.Item key="10">準備中</Menu.Item>
        </Menu.SubMenu>
      </Menu>
    </>
  );
};

export default SideMenu;
