import React from "react";
import { Button, Breadcrumb, Form, Input, InputNumber, Select, Row, Col } from "antd";
import "./Member.css"
import { addUser } from "../../../fetch/handler/user/userApiCallHandler"

const Member = (props) => {
  const [form] = Form.useForm();
  const { Option } = Select;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4, offset: 3},
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12, offset: 1 },
    },
  };

  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 7,
        offset: 17,
      },
    },
  };

  const onSubmit = (values) => {
    if (values !== undefined) {
      if (values.user.age === undefined) values.user.age = "";
      if (values.user.gender === undefined) values.user.gender = "";
      
      addUser(values.user, addUserSuccess, addUserFail);
    }
  };

  const addUserSuccess = (res) => {
    // 성공시 처리 기재
    alert(3333);
  }

  const addUserFail = (res) => {
    // 실패시 처리 기재
  }

  return (
    <>
      <div className="breadcrumb__area" style={{ marginBottom: 30 }}>
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>会員メニュー</Breadcrumb.Item>
          <Breadcrumb.Item>会員登録</Breadcrumb.Item>
        </Breadcrumb>
      </div>

      <div className="reg__member__area">
        <Form
          className="reg__member__form"
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onSubmit}
          scrollToFirstError
        >
          <Form.Item
            name={['user', 'id']}
            label="User_id"
            rules={[
              {
                required: true,
                message: 'IDを入力してください。',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={['user', 'password']}
            label="Password"
            rules={[
              {
                required: true,
                message: 'パスワードを入力してください。',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name={['user', 'email']}
            label="メール"
            rules={[
              {
                type: 'email',
                message: 'E-mail形式に合わせて入力してください。',
              },
              {
                required: true,
                message: 'E-mailを入力してください。',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name={['user', 'phoneNum']}
            label="電話番号"
            rules={[{ required: true, message: '電話番号を入力してください。' }]}
          >
            <Input placeholder="-なしで入力してください。" />
          </Form.Item>

          <Form.Item 
            name={['user', 'age']} 
            label="年齢" 
            rules={[{ type: 'number', min: 0, max: 99 }]}>
            <InputNumber />
          </Form.Item>

          <Form.Item
            name={['user', 'gender']} 
            label="性別"
          >
            <Select placeholder="性別をせんたくしてください。">
              <Option value="m">Male</Option>
              <Option value="f">Female</Option>
            </Select>
          </Form.Item>

          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
              会員登録
            </Button>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default Member;
