import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input, Button, Space } from 'antd';
import { DownOutlined, UserOutlined,MenuOutlined, EnvironmentOutlined, VerticalAlignBottomOutlined} from '@ant-design/icons';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

//const Map = () => {
 // const onClick = () => {
 //   console.log('Click')
  //}
  const onClick = () => {
    console.log('Click')
  }
  const onSearch = value => console.log(value);
  const { Search } = Input;

  class MapClass extends Component {
    
    render() {
      return (
        <div className='MapAPI'>
          <h1>地図から検索</h1>
          {/*<button color='green' text='add' onClick={onClick}>LV1</button>*/}
          <Search placeholder="input search text" allowClear onSearch={onSearch} style={{ width: 200 }} />          
          <Button onClick={onClick} icon={<EnvironmentOutlined />}>現在位置表示</Button>
          <Button onClick={onClick} icon={<VerticalAlignBottomOutlined />}>ダウンロード</Button>
          <Map
          google={this.props.google}
          zoom={15}
          initialCenter={{ lat:37.5,lng:127}}
      ></Map>
      </div>
      );
    }
  }
  export default GoogleApiWrapper({
    apiKey: "AIzaSyDM_6hhXdEiCkxku3ti8z03XdZ1pGI48B8",
  })(MapClass);
//Button.propTypes = {
//  text: PropTypes.string,
//  color: PropTypes.string,
//  onClick: PropTypes.func,
//}
//export default MapClass
