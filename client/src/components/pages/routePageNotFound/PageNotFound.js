import React from "react";
import { Link } from "react-router-dom";

import "./PageNotFound.css";

const PageNotFound = () => {
  return (
    <section className="PageNotFound">
      <h1>PageNotFound 404</h1>
      <Link to="/">메인 페이지로 돌아가기</Link>
    </section>
  );
};

export default PageNotFound;
