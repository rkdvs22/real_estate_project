import React from "react";
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { Menu, Dropdown, Button, message, Space, Tooltip } from 'antd';
import { DownOutlined, UserOutlined, MenuOutlined, ShopOutlined  } from '@ant-design/icons';
import './NavMenu.css'

function handleMenuClick(e) {
  message.info('Click on menu item.');
  console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick}>
    <Menu.Item key="1" icon={<UserOutlined />}>
      english
    </Menu.Item>
    <Menu.Item key="2" icon={<UserOutlined />}>
      日本語
    </Menu.Item>
    <Menu.Item key="3" icon={<UserOutlined />}>
      한국어
    </Menu.Item>
  </Menu>
);

const NavMenu = (props) => {
  const { SubMenu } = Menu;

  const handleClick = (e) => {
    console.log("click ", e);
  };

  const bt1Click = (e) => {
    console.log("click ", e);

  };

  return (
    <>
      {
      <Menu
        className="nav__menu__area"
        //defaultOpenKeys={['sub1']}
        onClick={handleClick}
        style={{ width: 256 }}
        defaultSelectedKeys={['1']}
          forceSubMenuRender={true}
        mode="inline"        
      >

        <Dropdown overlay={menu}>
          <Button>
              language <DownOutlined />
          </Button>
        </Dropdown>   
        <Button onClick={bt1Click} icon={<ShopOutlined />}>業者ページ</Button>
        
      </Menu>
      }
    </>
  )
};

export default NavMenu;
