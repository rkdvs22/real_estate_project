import React, {useState, useEffect} from 'react';
import { Layout } from 'antd';
import Routes from "./route";
import { BrowserRouter } from "react-router-dom";
// design for react component platform
import 'antd/dist/antd.css'
import './App.css'
//component
import SideMenu from "./components/sideMenu/SideMenu"
import NavMenu from './components/header/NavMenu'
import Footer from './components/footer/Footer'

import {getUserList} from './fetch/handler/user/userApiCallHandler'

function App() {
  // const [message, setMessage] = useState("");
  const [isCollapsed, setIsCollapsed] = useState(true);

  useEffect(() => {
    // init viewport
    const vh = window.innerHeight * 0.01;
    const wd = window.innerWidth * 0.01;
    // --vh && --wd custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    document.documentElement.style.setProperty('--wd', `${wd}px`);
    // test code
    getUserList(successCallBackTest, failCallBackTest);
  }, []);

  // test code
  const successCallBackTest = (res) => {
    console.log(res);
  }

  // test code
  const failCallBackTest = (res) => {
    
  }

  const sideMenu_toggle = () => {
    setIsCollapsed(!isCollapsed);
  };

  return (
    <BrowserRouter>
      <div className="App">
        <Layout className="antd__layout__in__app">
          <Layout.Sider className="app__slider" trigger={null} collapsible collapsed={isCollapsed}>
            <SideMenu 
              isCollapsed={isCollapsed}
              collapseToggle={sideMenu_toggle}
            />
          </Layout.Sider>
          <div className="site__layout">
            <Layout.Header className="app__top">
              <NavMenu/>
            </Layout.Header>
            <Layout.Content className="app__body">
              {/* application 컨텐트 페이지 루터 */}
              <Routes />
            </Layout.Content>
            <Layout.Footer className="app__bottom">
              <Footer></Footer>
            </Layout.Footer>
          </div>
        </Layout>
      </div>
    </BrowserRouter>
  );
}

export default App;
